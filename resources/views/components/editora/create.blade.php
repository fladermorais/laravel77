@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Criar uma Nova Editora</h3>
    </div>
  </div>
  
  <div class="card"> 
    <form action="{{ route('editora.store') }}" enctype="multipart/form-data" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-6">
            <label for="">Email</label>
            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
          </div>
          
          <div class="col-md-6">
            <label for="">Telefone</label>
            <input type="text" class="form-control" name="telefone" value="{{ old('telefone') }}">
          </div>
        </div>
        
        <div class="form-row">
          <div class="col-md-3">
            <label for="">Nome</label>
            <input type="text" class="form-control" name="nome" value="{{ old('nome') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">CNPJ</label>
            <input type="text" class="form-control" name="cnpj" value="{{ old('cnpj') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Localização</label>
            <input type="text" class="form-control" name="localização" value="{{ old('localização') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Contato</label>
            <input type="text" class="form-control" name="contato" value="{{ old('contato') }}">
          </div>
        </div>
      </div>
      
      <div class="card-footer">
        <div class="form-row">
          <button class="btn btn-primary" type="submit">Salvar</button>
        </div>  
      </div>
      
    </form>
  </div>
</div>
@endsection
