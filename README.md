## Estudando um pouco de laravel ##

# projeto Bookstore #

**laravel**

MVC

Organização das tabelas do banco de dados.

* Autores.

**Informações pessoais**.

- email
- telefone
- Nome
- Sobrenome
- Data de nasciemnto
- Naturalidade

_Dados técnicos_
- aid(id)

* Editoras.

**Informações e contato.**
- email
- telefone
- nome
- cnpj
- localização
- contato

_Dados técnicos_
- eid(id)

* Livros.

**Informações.**
- Título
- Autor(aid)
- Editora(edid)
- Descrição
- Ano
- Edição

_Dados técnicos_
- ldid(id)

# **Colaborando com o projeto**. #

Basta clonar o repositório para o local desejado.
 
Configurar o host(laravel77.exempo.test) no apache ou nginx para ele,o diretório dele configurar o banco de dados login senha e host.

* configurando:

Ajustar as variáveis do banco de dados, host, login, senha.
o usuário do banco de dados tem que ter acesso com permissões totais no banco.

* editando o arquivo de configuração:

cp .env.sample  .env

editor .env

creio ser necessário criar as tabelas com a ferramenta artisan do laravel:
php artisan migrate:fresh

Após isso basta colocar o sistema em execução:

php artisan up

Não alterei a página inicial do laravel
criei a view /home
o sistema será acessível a partir de "http://laravel77.exemplo.test/home"
>metas do estudo _laravel77_.


[books](https://codeberg.org/Linux77/laravel77)
Classes:

Livros
Autor
Editora
---

### BookStore ###

>Projeto base.

**Books**.
:
> Controle e registro de livros(obras literárias.

Classe:

Biblioteca.

Coleção baseada em books. com suporte a upload e download de obras literárias.

* Upload mediante cadastro e registro.

* Download e/ou aquisição da obra em formato pdf.

* Aquisição da obra em sua forma impressa.

_Leonardo de Araújo Lima_.

[Linux77](http://linux77.asl-sl.com.br)

**Academia do software livre São Lourenço**.

[A.S.L São Lourenço Website](http://www.asl-sl.com.br)



# **Parceiros**. #
[BRDSoft](http://www.brdsoft.com.br)
> Suporte em tecnologia.
> Infraestrutura em telecomunicações.
> Desenvolvimento de tecnologia em geral.
> Soluções em tecnologia.


