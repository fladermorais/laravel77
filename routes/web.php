<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\Admin\AutorController;
use App\Http\Controllers\LivroController;
use App\Http\Controllers\EditoraController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
})->name('home');
/*
Bem vindo.
*/
Route::get('/saudações', function() {
     return 'Bem vindo!';
});
/*
entrada dos dados
*/
Route::get('/cadastro', function() {
    return view('form');
});
/*
processamento dos dados
*/
Route::get('/posts/create', [PostController::class, 'postindex']);
/*
armazenamento dos dados
*/
Route::post('/save', [PostController::class, 'store']);
/*
página inicial do aplicativo
*/
Route::get('/home', function() {return view('home');});
/*
consulta em banco de dados
*/
Route::get('/dbstruct', [PostController::class, 'show']);
/* wrong way
livros

Route::get('/livros', function() {return view('livros');});
Route::get('/livros/cadastro', function() {return view('components.livros.input');});
Route::get('/livros/create', [PostController::class, 'livrosindex']);
Route::post('/livros/save', [PostController::class, 'store']);
autores
Route::get('/autores/create', [PostController::class, 'create_autor']);
Route::get('/autores/store' , [PostController::class, 'store_autor']);
Route::get('/autores/save' , [PostController::class, 'save_autor']);
//});
Route::get('/autores', [PostController::class, 'store_autor'])->name('s_autor');
//Route::get('/autores/cadastro',function() {return view('components.autores.input');});
//Route::post('/autores/save', [PostController::class, 'store']);
editoras
Route::get('/editoras', function() {return view('editoras');});
Route::get('/editoras/cadastro', function() {return view('components.editoras.input');});
Route::get('/editoras/create', [PostController::class, 'editoraindex']);
Route::post('/editoras/save', [PostController::class, 'store']);

/*
Autor
*/
Route::group(['prefix' => "autor"], function(){
    Route::get('/', [AutorController::class, 'index'])->name('autor.index');
    Route::get('/create', [AutorController::class, 'create'])->name('autor.create');
    Route::post('/store', [AutorController::class, 'store'])->name('autor.store');
    Route::get('/edit/{id}', [AutorController::class, 'edit'])->name('autor.edit');
    Route::put('/update/{id}', [AutorController::class, 'update'])->name('autor.update');
    Route::delete('/destroy/{id}', [AutorController::class, 'destroy'])->name('autor.destroy');
});

/*
Right Way revisão.
 Livros
*/

Route::group(['prefix' => "livro"], function(){
    Route::get('/', [LivroController::class, 'index'])->name('livro.index');
    Route::get('/create', [LivroController::class, 'create'])->name('livro.create');
    Route::post('/store', [LivroController::class, 'store'])->name('livro.store');
    Route::get('/edit/{id}', [LivroController::class, 'edit'])->name('livro.edit');
    Route::put('/update/{id}', [LivroController::class, 'update'])->name('livro.update');
    Route::delete('/destroy/{id}', [LivroController::class, 'destroy'])->name('livro.destroy');
});

/* Editoras
*/
Route::group(['prefix' => "editora"], function(){
    Route::get('/', [EditoraController::class, 'index'])->name('editora.index');
    Route::get('/create', [EditoraController::class, 'create'])->name('editora.create');
    Route::post('/store', [EditoraController::class, 'store'])->name('editora.store');
    Route::get('/edit/{id}', [EditoraController::class, 'edit'])->name('editora.edit');
    Route::put('/update/{id}', [EditoraController::class, 'update'])->name('editora.update');
    Route::delete('/destroy/{id}', [EditoraController::class, 'destroy'])->name('editora.destroy');
});

