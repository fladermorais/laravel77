@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Editar um Novo Livro</h3>
    </div>
  </div>
  
  <div class="card"> 
    <form action="{{ route('livro.update', $livro->id) }}" enctype="multipart/form-data" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-6">
            <label for="">Título</label>
            <input type="text" class="form-control" name="título" value="{{ $livro->título }}">
          </div>
          
          <div class="col-md-6">
            <label for="">Autor</label>
            {{-- <input type="text" class="form-control" name="autor" value="{{ $livro->aid }}"> --}}
            <select name="aid" id="aid" class="form-control">
              <option value="">Escolha uma opção</option>
              @foreach($autores as $autor)
              <option {{ $autor->id == $livro->aid ? "selected" : "" }} value="{{ $autor->id }}">{{ $autor->nome }}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="form-row">
          <div class="col-md-3">
            <label for="">Editora</label>
            <input type="text" class="form-control" name="eid" value="{{ $livro->eid }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Descrição</label>
            <input type="text" class="form-control" name="descrição" value="{{ $livro->descrição }}">
          </div>
          <div class="col-md-3">
            <label for="">Ano</label>
            <input type="text" class="form-control" name="ano" value="{{ $livro->ano }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Edição</label>
            <input type="text" class="form-control" name="edição" value="{{ $livro->edição }}">
          </div>
        </div>
      </div>

      <div class="card-footer">
        <div class="form-row">
          <button class="btn btn-primary" type="submit">Atualizar</button>
        </div>
      </div>
    </form>
  </div>
</div>

@endsection