<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Autor extends Model
{
    use HasFactory;
    
    protected $table = "autores";
    protected $fillable = ["nome", 'snome', 'email', 'telefone', 'dnasc', 'naturalidade'];
    
    public function rules()
    {
        return [
            "nome"          =>  "required",
            "snome"         =>  "required",
            "email"         =>  "required",
            "telefone"      =>  "required",
            "dnasc"         =>  "required",
            "naturalidade"  =>  "required",
        ];
    }
    
    public function newInfo($data)
    {
        $info = $this->create($data);
        return $info;
    }
    
    public function updateInfo($data)
    {
        $info = $this->update($data);
        return $info;
    }

    public function deleteInfo()
    {
        $info = $this->delete();
        return $info;
    }
}
