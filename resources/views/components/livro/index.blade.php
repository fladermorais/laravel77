@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card">
        <div class="card-header flex-row">
            <h3 class="mb-0">Livros</h3>
            <a class="btn btn-primary" href="{{route('livro.create')}}">Novo</a>
        </div>
    </div>
    
    <div class="card"> 
        <div class="card-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Título</th>
                        <th>Autor</th>
                        <th>Editora</th>
                        <th>Descrição</th>
                        <th>Ano</th>
                        <th>Edição</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($livros as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->título }}</td>
                        <td>{{ $value->aid }}</td>
                        <td>{{ $value->eid }}</td>
                        <td>{{ $value->descrição }}</td>
                        <td>{{ $value->ano }}</td>
                        <td>{{ $value->edição }}</td>

                        <td class="flex-row">
                            <a class="btn btn-info btn-sm left" href="{{route('livro.edit', $value->id)}}" title="Editar">Editar</a>
                            <form action="{{ route('livro.destroy', $value->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button onclick="return(confirm('Deseja realmente excluir?'))" class="btn btn-danger btn-sm" type="submit">Excluir</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
<a href="/home/"><b>Início.</b></a>
</div>
@endsection
