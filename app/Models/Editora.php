<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
class Editora extends Model
{
    use HasFactory;

	protected $table = 'editoras';
	protected $fillable = ['email', 'telefone', 'nome', 'cnpj', 'localização', 'contato'];
// início da definição e implementação da classe.
public function rules()
	{
		return [
		"email" => "required",
		"telefone" => "required",
		"nome" => "required",
		"cnpj" => "required",
		"localização" => "required",
		"contato" => "required"
		];
	}
//
public function newInfo($data)
	{
	$info = $this->create($data);
	return $info;
	}
//
public function updateInfo($data)
	{
	$info = $this->update($data);
	return $info;
	}
//
public function deleteInfo()
	{
	$info = $this-delete();
	return $info;
	}
// fim da definição da classe.
}
