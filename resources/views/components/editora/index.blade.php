@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card">
        <div class="card-header flex-row">
            <h3 class="mb-0">Editoras</h3>
            <a class="btn btn-primary" href="{{route('editora.create')}}">Novo</a>
        </div>
    </div>
    
    <div class="card"> 
        <div class="card-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Telefone</th>
                        <th>Nome</th>
                        <th>Localização</th>
                        <th>Contato</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($editoras as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->email }}</td>
                        <td>{{ $value->telefone }}</td>
                        <td>{{ $value->nome }}</td>
                        <td>{{ $value->localização }}</td>
                        <td>{{ $value->contato }}</td>
                        <td class="flex-row">
                            <a class="btn btn-info btn-sm left" href="{{route('editora.edit', $value->id)}}" title="Editar">Editar</a>
                            <form action="{{ route('editora.destroy', $value->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button onclick="return(confirm('Deseja realmente excluir?'))" class="btn btn-danger btn-sm" type="submit">Excluir</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>
<a href="/home/"><br>Início.</b></a>
</div>
@endsection

