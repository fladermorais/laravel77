 <form action="/posts/create" method="GET">
<h3> Cadastro de obras literárias.</h3><br>

<label for="n">Título da obra:</label>
<input id="n" type="text" name="nome" class="@error('nome') is-invalid @enderror"><br>

<label for="y">Ano:</label>
<input id="y" type="text" name="ano" class="@error('ano') is-invalid @enderror"><br>

<label for="e">Editora:</label>
<input id="e" type="text" name="editora" class="@error('editora') is-invalid @enderror"><br>

<label for="g">Gênero:</label>
<input id="g" type="text" name="gênero" class="@error('gênero') is-invalid @enderror"><br>

<label for="a">Autor:</label>
<input id="a" type="text" name="autor" class="@error('autor') is-invalid @enderror"><br>
<input type="submit" name="enviar" value="registrar">
</form>
<a href="/home">Início.</a>
