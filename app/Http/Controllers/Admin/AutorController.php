<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Autor;
use Illuminate\Support\Facades\Validator;

class AutorController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $autores = Autor::all();
        return view('Admin.autor.index', compact('autores'));
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('Admin.autor.create');
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $data = $request->all();
        $autor = new Autor;
        $validator = Validator::make($data, $autor->rules());
        
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }
        
        $response = $autor->newInfo($data);
        if($response){
            return redirect()->route('autor.index');
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        $autor = Autor::find($id);
        if(!isset($autor)){
            return back();
        }
        
        return view('Admin.autor.edit', compact('autor'));
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $autor = Autor::find($id);
        if(!isset($autor)){
            return back();
        }
        $validator = Validator::make($data, $autor->rules());
        
        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        }
        
        $response = $autor->updateInfo($data);
        if($response){
            return redirect()->route('autor.index');
        } else {
            return redirect()->route('autor.index');
        }
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $autor = Autor::find($id);
        if(!isset($autor)){
            return back();
        }
        $response = $autor->deleteInfo();
        return back();
    }
}
