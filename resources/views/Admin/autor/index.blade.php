@extends('layouts.app')
@section('content')

<div class="container">
    <div class="card">
        <div class="card-header flex-row">
            <h3 class="mb-0">Autores</h3>
            <a class="btn btn-primary" href="{{route('autor.create')}}">Novo</a>
        </div>
    </div>
    
    <div class="card"> 
        <div class="card-body">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th>ID</th>
                        <th>Nome</th>
                        <th>Sobre Nome</th>
                        <th>Email</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($autores as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->nome }}</td>
                        <td>{{ $value->snome }}</td>
                        <td>{{ $value->email }}</td>
                        <td class="flex-row">
                            <a class="btn btn-info btn-sm left" href="{{route('autor.edit', $value->id)}}" title="Editar">Editar</a>
                            <form action="{{ route('autor.destroy', $value->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button onclick="return(confirm('Deseja realmente excluir?'))" class="btn btn-danger btn-sm" type="submit">Excluir</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>   
    </div>
<a href="/home/"><b>Início.</b></a>
</div>
@endsection
