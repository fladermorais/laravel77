<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Editora;
use Illuminate\Support\Facades\Validator;
class EditoraController extends Controller
{
    //

//
public function index()
	{
	$editoras = Editora::all();
//	return view('test',['var'=>"editora index..."]);
  return view('components.editora.index', compact('editoras'));
	}
//
public function create()
	{
//	return view('test',['var'=>"editora create..."]);
  return view('components.editora.create');
	}
//
public function show() {
	return view('test',['var'=>"editora show..."]);
//  return view('components.editora.index', compact('editoras'));
	}
//
public function store(Request $request)
 	{
        $data = $request->all();
        $editora = new Editora;
        $validator = Validator::make($data, $editora->rules());

        if($validator->fails()){
            return back()->withInput()->withErrors($validator);
        		}
        	$response = $editora->newInfo($data);
        	if($response){
            	return redirect()->route('editora.index');
        		}
    	}

//	return view('test',['var'=>"editora store..."]);
//
public function edit($id)
  		{
        		$editora = Editora::find($id);
        		if(!isset($editora)){
            		return back();
        }
        return view('components.editora.edit', compact('editoras'));
    }


//{
//	return view('test',['var'=>"editora edit..."]);
//	}
//
public function update(Rquest $request, $id)
 	{
        $data = $request->all();
        $editora = Editora::find($id);
        if(!isset($editora)){
            return back();
        }
        	$validator = Validator::make($data, $editora->rules());
        	if($validator->fails()){
            	return back()->withInput()->withErrors($validator);
        	}

        $response = $editora->updateInfo($data);
        if($response){
        	    return redirect()->route('editora.index');
        	} else {
            	return redirect()->route('editora.index');
        	}
    }
//{
//	return view('test',['var'=>"editora update..."]);
//	}
//
public function destroy()
 {
        $editora = Editora::find($id);
        if(!isset($editora)){
            return back();
        }
        $response = $editora->deleteInfo();
        return back();
    }
}

