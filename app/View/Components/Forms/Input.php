<?php

namespace App\View\Components\Forms;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
<label for="email">email:</label><br/>

<input id="email" type="text" class="@error('email') is-invalid @enderror"><br/>

<label for="telefone">Telefone:</label><br/>

<label for="nome">Nome:</label><br/>

<label for="snome">Sobrenome:</label><br/>

<label for="dnasc">Data de nascimento:</label><br/>

<label for="naturalidade">Naturalidade::</label><br/>

        return view('components.forms.input');
    }
}
