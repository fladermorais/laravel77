<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Livro extends Model
{
	use HasFactory;
	
	protected $table = "livros";
	protected $fillable = ["título", 'aid', 'eid', 'descrição', 'ano', 'edição'];
	// início da definição e implementação da classe.
	public function rules()
	{
		return [
			"título" 		=> "required",
			"aid" 			=> "required",
			"eid" 			=> "required",
			"descrição" => "required",
			"ano" 			=> "required",
			"edição"		=> "required"
		];
	}
	
	public function newInfo($data)
	{
		$info = $this->create($data);
		return $info;
		//
	}
	public function updateInfo($data)
	{
		$info = $this->update($data);
		return $info;
		//
	}
	
	public function deleteInfo()
	{
		$info = $this->delete();
		return $info;
		//
	}
	
	// fim da definição da classe.
}
