@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Editar um Novo Autor</h3>
    </div>
  </div>
  
  <div class="card"> 
    <form action="{{ route('autor.update', $autor->id) }}" enctype="multipart/form-data" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-6">
            <label for="">Nome</label>
            <input type="text" class="form-control" name="nome" value="{{ $autor->nome }}">
          </div>
          
          <div class="col-md-6">
            <label for="">Sobre Nome</label>
            <input type="text" class="form-control" name="snome" value="{{ $autor->snome }}">
          </div>
        </div>
        
        <div class="form-row">
          <div class="col-md-3">
            <label for="">Email</label>
            <input type="email" class="form-control" name="email" value="{{ $autor->email }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Telefone</label>
            <input type="text" class="form-control" name="telefone" value="{{ $autor->telefone }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Data NAscimento</label>
            <input type="date" class="form-control" name="dnasc" value="{{ $autor->dnasc }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Naturalidade</label>
            <input type="text" class="form-control" name="naturalidade" value="{{ $autor->naturalidade }}">
          </div>
        </div>
      </div>
      
      <div class="card-footer">
        <div class="form-row">
          <button class="btn btn-primary" type="submit">Salvar</button>
        </div>  
      </div>
      
    </form>
  </div>
</div>
@endsection