@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Editar um Novo Livro</h3>
    </div>
  </div>
  
  <div class="card"> 
    <form action="{{ route('editora.update', $editora->id) }}" enctype="multipart/form-data" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-6">
            <label for="">Email</label>
            <input type="email" class="form-control" name="email" value="{{ $editora->email }}">
          </div>

          <div class="col-md-6">
            <label for="">Telefone</label>
            <input type="text" class="form-control" name="telefone" value="{{ $editora->telefone }}">
          </div>
        </div>

        <div class="form-row">
          <div class="col-md-3">
            <label for="">Nome</label>
            <input type="text" class="form-control" name="nome" value="{{ $editora->nome }}">
          </div>

          <div class="col-md-3">
            <label for="">CNPJ</label>
            <input type="text" class="form-control" name="cnpj" value="{{ $editora->cnpj }}">
          </div>
          <div class="col-md-3">
            <label for="">Localização</label>
            <input type="text" class="form-control" name="localização" value="{{ $editora->localização }}">
          </div>

          <div class="col-md-3">
            <label for="">Contato</label>
            <input type="text" class="form-control" name="contato" value="{{ $editora->contato }}">
          </div>
        </div>
      </div>

