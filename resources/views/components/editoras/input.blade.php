<h1>Cadastrando editoras</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="GET" action="{{ url('/editoras/create') }}">
<h3>Cadastro de editoras</h3>
<label for="email">email:</label>
<input id="email" type="text"><br/>

<label for="telefone">Telefone:</label>
<input id="telefone" type="text"><br/>

<label for="nome">Nome:</label>
<input id="nome" type="text"><br/>
<label for="cnpj">CNPJ:</label>
<input id="cnpj" type="text"><br/>

<label for="localização">Localização:</label>
<input id="localização" type="text"><br/>

<label for="contato">Contato:</label>
<input id="contato" type="text" class="@error('contato') is-invalid @enderror">
<input type="submit" name="enviar" value="cadastrar_editora">
</form>
<a href="/home">Página inicial</a>
