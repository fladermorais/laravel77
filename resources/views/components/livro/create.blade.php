@extends('layouts.app')
@section('content')

<div class="container">
  <div class="card">
    <div class="card-header">
      <h3 class="mb-0">Criar um Novo Livro</h3>
    </div>
  </div>
  
  <div class="card"> 
    <form action="{{ route('livro.store') }}" enctype="multipart/form-data" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-row">
          <div class="col-md-6">
            <label for="">Título</label>
            <input type="text" class="form-control" name="título" value="{{ old('título') }}">
          </div>
          
          <div class="col-md-6">
            <label for="">Autor</label>
            {{-- <input type="text" class="form-control" name="aid" value="{{ old('aid') }}"> --}}
            <select name="aid" id="aid" class="form-control">
              <option value="">Escolha uma opção</option>
              @foreach($autores as $autor)
              <option value="{{ $autor->id }}">{{ $autor->nome }}</option>
              @endforeach
            </select>
          </div>
        </div>
        
        <div class="form-row">
          <div class="col-md-3">
            <label for="">Editora</label>
            <input type="text" class="form-control" name="eid" value="{{ old('eid') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Descrição</label>
            <input type="text" class="form-control" name="descrição" value="{{ old('descrição') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Ano</label>
            <input type="text" class="form-control" name="ano" value="{{ old('ano') }}">
          </div>
          
          <div class="col-md-3">
            <label for="">Edição</label>
            <input type="text" class="form-control" name="edição" value="{{ old('edição') }}">
          </div>
        </div>
      </div>
      
      <div class="card-footer">
        <div class="form-row">
          <button class="btn btn-primary" type="submit">Salvar</button>
        </div>  
      </div>
      
    </form>
  </div>
</div>
@endsection
