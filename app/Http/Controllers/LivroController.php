<?php

namespace App\Http\Controllers;

use App\Models\Autor;
use Illuminate\Http\Request;
use App\Models\Livro;
use Illuminate\Support\Facades\Validator;
class LivroController extends Controller
{
	//
	
	//
	public function index()
	{
		$livros = Livro::all();
		//	return view('test',['var'=>"livro index..."]);
		return view('components.livro.index', compact('livros'));
	}
	//
	public function create()
	{
		// Pegando o valor dos autores
		$autores = Autor::orderBy('nome', 'asc')->get();

		return view('components.livro.create', compact('autores'));
	}
	//
	public function show() {
		return view('test',['var'=>"livro show..."]);
		//  return view('components.livro.index', compact('livros'));
	}
	//
	public function store(Request $request)
	{
		$data = $request->all();
		$livro = new Livro;
		$validator = Validator::make($data, $livro->rules());
		
		if($validator->fails()){
			return back()->withInput()->withErrors($validator);
		}
		$response = $livro->newInfo($data);
		if($response){
			return redirect()->route('livro.index');
		}
	}
	
	//	return view('test',['var'=>"livro store..."]);
	//
	public function edit($id)
	{
		$livro = Livro::find($id);
		if(!isset($livro)){
			return back();
		}
		// Pegando o valor dos autores
		$autores = Autor::orderBy('nome', 'asc')->get();

		return view('components.livro.edit', compact('livro', 'autores'));
	}
	
	
	//{
		//	return view('test',['var'=>"livro edit..."]);
		//	}
		//
		public function update(Request $request, $id)
		{
			$data = $request->all();
			// dd($data);

			$livro = Livro::find($id);
			if(!isset($livro)){
				return back();
			}
			$validator = Validator::make($data, $livro->rules());
			if($validator->fails()){
				return back()->withInput()->withErrors($validator);
			}
			
			$response = $livro->updateInfo($data);
			if($response){
				return redirect()->route('livro.index');
			} else {
				return redirect()->route('livro.index');
			}
		}
		//{
			//	return view('test',['var'=>"livro update..."]);
			//	}
			//
			public function destroy()
			{
				$livro = Livro::find($id);
				if(!isset($livro)){
					return back();
				}
				$response = $livro->deleteInfo();
				return back();
			}
		}
		
		